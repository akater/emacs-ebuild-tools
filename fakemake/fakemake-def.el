;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'ebuild-tools
  authors "Dima Akater"
  first-publication-year-as-string "2020"
  org-files-in-order '("ebuild-tools-macs"
                       "ebuild-tools-core"
                       "ebuild-tools-repositories"
                       "ebuild-tools-ebuild-mode-extensions"
                       "ebuild-tools-rest"
                       "ebuild-tools")
  site-lisp-config-prefix "50"
  license "GPL-3")

(advice-add 'fakemake-test :before
            (lambda () (require 'org-src-elisp-extras)))
