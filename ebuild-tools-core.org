# -*- coding: utf-8; mode: org-development-elisp; -*-
#+title: ebuild-tools-core
#+subtitle: Part of the =ebuild-tools= Emacs Lisp package
#+author: =#<PERSON akater A24961DE3ADD04E057ADCF4599555CE6F2E1B21D>=
#+property: header-args :tangle ebuild-tools-core.el :lexical t
#+startup: nologdone show2levels
#+todo: TODO(t@) HOLD(h@/!) | DONE(d@)
#+todo: BUG(b@/!) | FIXED(x@)
#+todo: TEST(u) TEST-FAILED(f) | TEST-PASSED(p)
#+todo: DEPRECATED(r@) | OBSOLETE(o@)

* Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'mmxx-macros-anaphora))
(eval-when-compile (require 'mmxx-macros-defmacro))
(require 'akater-misc-strings)
(require 'akater-misc-buffers)
(require 'shmu-wrap)
(require 'shmu-env)
(eval-when-compile (require 'akater-misc-buffers-macs))
(declare-function cl-member "cl-seq")
(declare-function cl-remove-if-not "cl-seq")
#+end_src

#+begin_src elisp :tangle (if (memq 'test ob-flags) "ebuild-tools-core-tests.el" "") :results none
(require 'akater-misc-buffers-macs)
#+end_src

* Constants
#+begin_src elisp :results none
(defconst ebuild-suffix ".ebuild")
(defconst ebuild-suffix-length (length ebuild-suffix))
#+end_src

* Global Variables
I'm not sure if ~defcustom~ is actually preferrable to ~defvar~ here.

#+begin_src elisp :results none
(defgroup ebuild-tools nil
  "Manage ebuilds from within Emacs."
  :group 'external)
#+end_src

#+begin_src elisp :results none
(defcustom ebuild-tools-catpkg-get-function 'gentoo-cache-get-package-names
  "Function of 0 arguments that returns the completions list
of CATPKGSs on each call to functions defined with `defun-catpkgs'.

If you re-set this, don't forget to re-emerge and reload in Emacs
all packages that depend on `ebuild-tools-macs'."
  :type 'function
  :group 'ebuild-tools)
#+end_src

#+begin_src elisp :results none
(defvar ebuild-tools-catpkg-history-list nil
  "History when completing catpkg")
#+end_src

#+begin_src elisp :results none
(defvar ebuild-tools-catpkgs-history-list nil
  "History when completing catpkgs")
#+end_src

#+begin_src elisp :results none
(defcustom ebuild-tools-build-logs-directory "/var/log/portage/build-logs/"
  "Where build logs produced by the “ebuild” command are located."
  :type 'directory
  :group 'ebuild-tools)
#+end_src

* ebuild-tools-catpkg-from-ebuildname
** Notes
ebuildname is catpkg-pvr-repo

** Prerequisites
*** char-in-range-p
**** Examples
***** Basic Examples
****** TEST-PASSED Trivial char-in-range-p example
#+begin_src elisp :tangle no :results code :wrap example elisp :keep-expected t
(macroexpand-all
 `(char-in-range-p (aref string i) (any (?a . ?z) ?-
                                        (?0 . ?9) (?A . ?Z)
                                        ?_ ?+ ?\.)))
#+end_src

#+EXPECTED:
#+begin_example elisp
(let ((char-0 (aref string i)))
  (or (<= ?a char-0 ?z)
      (char-equal ?- char-0)
      (<= ?0 char-0 ?9)
      (<= ?A char-0 ?Z)
      (char-equal ?_ char-0)
      (char-equal ?+ char-0)
      (char-equal ?\. char-0)))
#+end_example

**** Definition
#+begin_src elisp :results none
(defmacro char-in-range-p (char rx-char-or-char-range)
  (cl-etypecase rx-char-or-char-range
    ((cl-cons character character)
     `(<= ,(car rx-char-or-char-range) ,char ,(cdr rx-char-or-char-range)))
    (character
     `(char-equal ,rx-char-or-char-range ,char))
    ((cl-cons (eql any) cons)
     (let ((char-g (let0 (gensym-counter) (gensym "char-"))))
       `(let ((,char-g ,char))
          (or ,@(amapcar `(char-in-range-p ,char-g ,it)
                         (cdr rx-char-or-char-range))))))))
#+end_src

*** with-parsed-ebuildname
**** Examples
***** Basic Examples
****** TEST-PASSED Trivial
#+begin_src elisp :tangle no :results code :wrap example elisp
(with-parsed-ebuildname "app/hey-0::gentoo"
  pn)
#+end_src

#+EXPECTED:
#+begin_example elisp
"hey"
#+end_example

**** Definition
#+begin_src elisp :results none
(defmacro-mmxx/20230710 with-parsed-ebuildname (string &body body
                                              &gensym string
                                              (length `(length ,string))
                                              (i 0 nil) j)
  `(progn
     (cl-assert (cl-plusp ,length))
     ;; category starts here
     (awhen (cl-member (aref ,string ,i) '(?- ?\. ?+) :test #'char-equal)
       (error "Category name begins with “%c” --- see PMS 3.1.1 Category names"
              (car it)))
     (while (char-in-range-p (aref ,string ,i) (any (?a . ?z) ?-
                                                    (?0 . ?9) (?A . ?Z)
                                                    ?_ ?+ ?\.))
       (cl-incf ,i))
     (unless (char-equal ?/ (aref ,string ,i))
       (error "Category name not followed with / in atom %s"
              'atom))
     (cl-incf ,i)
     ;; category ends here
     (awhen (cl-member (aref ,string ,i) '(?- ?+) :test #'char-equal)
       (error "Package name begins with %c --- see PMS 3.1.2 Package names"
              (car it)))
     ;; now we go from the end
     (let ((,j (1- ,length))
           (category-end (1- ,i)) (package-name-beginning ,i))
       (ignore category-end package-name-beginning)
       (while (char-in-range-p (aref ,string ,j) (any (?a . ?z) ?-
                                                      (?A . ?Z) ?_
                                                      (?0 . ?9)))
         (cl-decf ,j))
       (let ((repository-name-beginning (1+ ,j)))
         (ignore repository-name-beginning)
         (if (and (char-equal ?: (aref ,string ,j))
                  (char-equal ?: (aref ,string (cl-decf ,j))))
             (cl-decf ,j)
           (error "Repository name missing or contains forbidden character %c"
                  (aref ,string ,j)))
         (let* ((pvr-end (1+ ,j)) (pv-end 0)
                (revision-beginning
                 (akater-misc--extra-string-match-p ,string
                   (seq "-r" (one-or-more (any (?0 . ?9))))
                   :start (1- pvr-end)
                   :from-end t)))
           (setq pv-end (or revision-beginning pvr-end))
           (setq ,j
                 (akater-misc--extra-string-match-p ,string
                   (seq (one-or-more (any (?0 . ?9)))
                        (zero-or-more ?\. (one-or-more (any (?0 . ?9))))
                        (zero-or-one (any (?a . ?z)))
                        (zero-or-more ?_ (or "alpha" "beta"
                                             "pre" ;; pre-release
                                             "rc" ;; release candidate
                                             "p"  ;; patch
                                             )
                                      (one-or-more (any (?0 . ?9)))))
                   :from-end t
                   :start (1- pv-end)))
           (unless ,j (error "No proper version in ebuildname “%s”"
                             ,string))
           (unless (char-equal ?- (aref ,string (cl-decf ,j)))
             (error "Incorrect character before version: %c"
                    (aref ,string ,j)))
           (let* ((pn-end ,j) (pn (substring ,string ,i pn-end)))
             (unless (string-match-p (rx (any (?a . ?z) ?-
                                              (?0 . ?9) (?A . ?Z)
                                              ?_ ?+))
                                     pn)
               (error "Package name contains invalid characters: “%s”" pn))
             (let ((pv (substring ,string ,i pv-end)))
               (ignore pv)
               ,@body)))))))
#+end_src

*** parse-ebuildname
**** Notes
Mostly for the sake of (fast) tests for ~with-parsed-ebuildname~.

**** Examples
***** Basic Examples
****** TEST-PASSED Parse ebuildname
#+begin_src elisp :tangle no :results code :wrap example elisp :keep-expected t
(ebulid-tools--parse-ebuildname
 "app-emacs/pdf-tools-0.91_pre20211008-r111115::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
( :category "app-emacs" :package-name "pdf-tools"
  :pv "pdf-tools-0.91_pre20211008" :rev "-r111115"
  :pvr "pdf-tools-0.91_pre20211008-r111115"
  :repo "akater")
#+end_example

**** Definition
#+begin_src elisp :results none
(defun ebulid-tools--parse-ebuildname (string)
  (with-parsed-ebuildname string
    (list :category (substring string 0 category-end)
          :package-name pn
          :pv pv
          :rev (when revision-beginning
                 (substring string revision-beginning pvr-end))
          :pvr (if revision-beginning
                   (substring string package-name-beginning pvr-end)
                 pv)
          :repo (substring string repository-name-beginning))))
#+end_src

**** Tests
***** TEST-PASSED full stack
#+begin_src elisp :tangle no :results code :wrap example elisp :keep-expected t
(ebulid-tools--parse-ebuildname
 "app-emacs/pdf-tools-0.91_pre20211008-r111115::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
( :category "app-emacs" :package-name "pdf-tools"
  :pv "pdf-tools-0.91_pre20211008" :rev "-r111115"
  :pvr "pdf-tools-0.91_pre20211008-r111115"
  :repo "akater")
#+end_example

***** TEST-PASSED full stack, two prefixes
#+begin_src elisp :tangle no :results code :wrap example elisp :keep-expected t
(ebulid-tools--parse-ebuildname
 "app-emacs/pdf-tools-0.91_pre20211008_alpha1-r111115::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
( :category "app-emacs" :package-name "pdf-tools"
  :pv "pdf-tools-0.91_pre20211008_alpha1" :rev "-r111115"
  :pvr "pdf-tools-0.91_pre20211008_alpha1-r111115"
  :repo "akater")
#+end_example

***** TEST-PASSED full stack, sans revision
#+begin_src elisp :tangle no :results code :wrap example elisp :keep-expected t
(ebulid-tools--parse-ebuildname "app-emacs/pdf-tools-0.91_pre20211008::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
( :category "app-emacs" :package-name "pdf-tools"
  :pv "pdf-tools-0.91_pre20211008" :rev nil
  :pvr "pdf-tools-0.91_pre20211008"
  :repo "akater")
#+end_example

***** TEST-PASSED full stack, two prefixes, sans revision
#+begin_src elisp :tangle no :results code :wrap example elisp :keep-expected t
(ebulid-tools--parse-ebuildname "app-emacs/pdf-tools-0.91_pre20211008_alpha1::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
( :category "app-emacs" :package-name "pdf-tools"
  :pv "pdf-tools-0.91_pre20211008_alpha1" :rev nil
  :pvr "pdf-tools-0.91_pre20211008_alpha1"
  :repo "akater")
#+end_example

** Examples
*** Basic Examples
**** TEST-PASSED Get package name from ebuildname
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "virtual/emacs-24::gentoo")
#+end_src

#+EXPECTED:
#+begin_example elisp
"virtual/emacs"
#+end_example

**** TEST-PASSED Get package name from ebuildname with revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "x11-base/xorg-server-1.20.5-r2::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
"x11-base/xorg-server"
#+end_example

**** TEST-PASSED Get package name from ebuildname without revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "x11-base/xorg-server-1.20.5::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
"x11-base/xorg-server"
#+end_example

*** Scope
**** TEST-PASSED Error on incorrect category name
#+begin_src elisp :tangle no :results code :wrap example elisp
(condition-case err
    (ebuild-tools-catpkg-from-ebuildname "+virtual/+emacs-24::gentoo")
  (t err))
#+end_src

#+EXPECTED:
#+begin_example elisp
(error "Category name begins with “+” --- see PMS 3.1.1 Category names")
#+end_example

**** TEST-PASSED Error on incorrect package name
#+begin_src elisp :tangle no :results code :wrap example elisp
(condition-case err
    (ebuild-tools-catpkg-from-ebuildname "virtual/+emacs-24::gentoo")
  (t err))
#+end_src

#+EXPECTED:
#+begin_example elisp
(error "Package name begins with + --- see PMS 3.1.2 Package names")
#+end_example

** Definition
#+begin_src elisp :results none
(defun ebuild-tools-catpkg-from-ebuildname (string)
  (with-parsed-ebuildname string
    (substring string 0 pn-end)))
#+end_src

** Tests
**** TEST-PASSED full stack
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "app-emacs/pdf-tools-0.91_pre20211008-r111115::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
"app-emacs/pdf-tools"
#+end_example

**** TEST-PASSED full stack twice
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "app-emacs/pdf-tools-0.91_pre20211008_alpha1-r111115::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
"app-emacs/pdf-tools"
#+end_example

**** TEST-PASSED full stack sans revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "app-emacs/pdf-tools-0.91_pre20211008::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
"app-emacs/pdf-tools"
#+end_example

**** TEST-PASSED full stack twice sans revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "app-emacs/pdf-tools-0.91_pre20211008_alpha1::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
"app-emacs/pdf-tools"
#+end_example

**** TEST-PASSED n.m.k.l version, repository
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "app-shells/dash-0.5.11.2::gentoo")
#+end_src

#+EXPECTED:
#+begin_example elisp
"app-shells/dash"
#+end_example

**** TEST-PASSED n.m version, revision, repository
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "x11-base/xorg-server-1.20-r2::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
"x11-base/xorg-server"
#+end_example

**** TEST-PASSED n.m version, revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "x11-base/xorg-server-1.20-r2::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
"x11-base/xorg-server"
#+end_example

**** TEST-PASSED n version, revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-ebuildname "x11-base/xorg-server-0-r2::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
"x11-base/xorg-server"
#+end_example

* ebuild-tools-catpkg-from-atom
** catpkg is officially known as “qualified package name”
according to PMS 3.1.2 Package names

** This “atom” is not really atom
In the sense that it has all attributes of atom except atom prefix and slot, but can have repo id in the end (which atoms don't have).

** Examples
*** Basic Examples
**** TEST-PASSED Get package name from atom
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-atom "=virtual/emacs-24")
#+end_src

#+EXPECTED:
#+begin_example elisp
"virtual/emacs"
#+end_example

**** TEST-PASSED Get package name from atom with revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-atom "=x11-base/xorg-server-1.20.5-r2")
#+end_src

#+EXPECTED:
#+begin_example elisp
"x11-base/xorg-server"
#+end_example

**** TEST-PASSED Get package name from atom with revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-atom ">=x11-base/xorg-server-1.20.5")
#+end_src

#+EXPECTED:
#+begin_example elisp
"x11-base/xorg-server"
#+end_example

**** TEST-PASSED Get package name from atom without extended prefixes
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-atom "virtual/emacs-24")
#+end_src

#+EXPECTED:
#+begin_example elisp
"virtual/emacs"
#+end_example

** Definition
#+begin_src elisp :results none
(defun ebuild-tools-catpkg-from-atom (string)
  (cl-assert (cl-plusp (length string)))
  (akater-misc-remove-regexps-in-string*
   '((rx string-start
         (or "="
             "<"
             ">="))
     (rx "::" (one-or-more anything) string-end)
     (rx "-"
         (zero-or-one (repeat 0 4 (one-or-more digit) ".")
                      (one-or-more digit))
         (zero-or-one "-r" (one-or-more digit))
         string-end))
   string))
#+end_src

** Tests
**** pre
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-atom "app-emacs/pdf-tools-0.91_pre20211008::akater")
#+end_src

**** TEST-PASSED n.m.k.l version, repository
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-atom "app-shells/dash-0.5.11.2::gentoo")
#+end_src

#+EXPECTED:
#+begin_example elisp
"app-shells/dash"
#+end_example

**** TEST-PASSED n.m version, revision, repository
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-atom "=x11-base/xorg-server-1.20-r2::akater")
#+end_src

#+EXPECTED:
#+begin_example elisp
"x11-base/xorg-server"
#+end_example

**** TEST-PASSED n.m version, revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-atom "=x11-base/xorg-server-1.20-r2")
#+end_src

#+EXPECTED:
#+begin_example elisp
"x11-base/xorg-server"
#+end_example

**** TEST-PASSED n version, revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-catpkg-from-atom "=x11-base/xorg-server-0-r2")
#+end_src

#+EXPECTED:
#+begin_example elisp
"x11-base/xorg-server"
#+end_example

* ebuild-tools-package-name-from-atom
** Examples
*** Basic Examples
**** TEST-PASSED Get package name from atom
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-package-name-from-atom "=virtual/emacs-24")
#+end_src

#+EXPECTED:
#+begin_example elisp
"emacs"
#+end_example

**** TEST-PASSED Get package name from atom with revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-package-name-from-atom "=x11-base/xorg-server-1.20.5-r2")
#+end_src

#+EXPECTED:
#+begin_example elisp
"xorg-server"
#+end_example

**** TEST-PASSED Get package name from atom with revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-package-name-from-atom ">=x11-base/xorg-server-1.20.5")
#+end_src

#+EXPECTED:
#+begin_example elisp
"xorg-server"
#+end_example

**** TEST-PASSED Get package name from atom without extended prefixes
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-package-name-from-atom "virtual/emacs-24")
#+end_src

#+EXPECTED:
#+begin_example elisp
"emacs"
#+end_example

** Definition
#+begin_src elisp :results none
(defun ebuild-tools-package-name-from-atom (string)
  (cl-assert (cl-plusp (length string)))
  (akater-misc-remove-regexps-in-string*
   '((rx string-start
         (or (zero-or-one "=")
             (zero-or-one "<")
             (zero-or-one ">="))
         (one-or-more (not ?/)) "/")
     (rx "-"
         (zero-or-one (repeat 0 2 (one-or-more digit) ".")
                      (one-or-more digit))
         (zero-or-one "-r" (one-or-more digit))
         string-end))
   string))
#+end_src

** Tests
**** TEST-PASSED n.m version, revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-package-name-from-atom "=x11-base/xorg-server-1.20-r2")
#+end_src

#+EXPECTED:
#+begin_example elisp
"xorg-server"
#+end_example

**** TEST-PASSED n version, revision
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-package-name-from-atom "=x11-base/xorg-server-0-r2")
#+end_src

#+EXPECTED:
#+begin_example elisp
"xorg-server"
#+end_example

* ebuild-tools-repository-name-from-atom
** Examples
*** Basic Examples
**** TEST-PASSED Trivial ~ebuild-tools-repository-name-from-atom~ example
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-repository-name-from-atom "::gentoo")
#+end_src

#+EXPECTED:
#+begin_example elisp
"gentoo"
#+end_example

** Definition
#+begin_src elisp :results none
(defun ebuild-tools-repository-name-from-atom (string)
  (cl-assert (cl-plusp (length string)))
  (let ((prefix "::"))
    (awhen (string-match (rx (literal prefix) (one-or-more (not ?:)) string-end) string)
      (substring string (+ (length prefix) it)))))
#+end_src

* ebuild-tools-version-from-atom
** Examples
*** Basic Examples
**** TEST-PASSED Trivial ~ebuild-tools-version-from-atom~ example
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-version-from-atom "=x11-base/xorg-server-1.20.5-r2::gentoo")
#+end_src

#+EXPECTED:
#+begin_example elisp
"1.20.5-r2"
#+end_example

** Definition
#+begin_src elisp :results none
(defun ebuild-tools-version-from-atom (string)
  (cl-assert (cl-plusp (length string)))
  (let ((string-sans-repository (akater-misc-remove-regexps-in-string*
                                 '((rx "::"
                                       (one-or-more anything)
                                       string-end))
                                 string))
        (prefix "-"))
    (awhen (string-match-p
            (rx (literal prefix)
                (zero-or-one (repeat 0 2 (one-or-more digit) ".")
                             (one-or-more digit))
                (zero-or-one "-r" (one-or-more digit))
                string-end)
            string-sans-repository)
      (substring string-sans-repository (+ (length prefix) it)))))
#+end_src

* all-useflags
** Prerequisites
*** faces
**** I have to do it like this
#+begin_src elisp :results none
(require 'term)
(defface ebuild-useflag-enabled
  `((t
     :inherit default
     :weight bold
     :foreground ,(face-foreground 'term-color-red)))
  "Enabled USE flag"
  :group 'ebuild-tools)
(defface ebuild-useflag-disabled
  `((t
     :inherit default
     :weight bold
     :foreground ,(face-foreground 'term-color-blue)))
  "Disabled USE flag"
  :group 'ebuild-tools)
#+end_src

**** I'd rather do it this way
#+begin_src elisp :tangle no :results none :eval never
(eval-when-compile (require 'term))
(defface ebuild-useflag-enabled
  `((t
     :inherit default
     :weight bold
     :foreground ,(eval-when-compile (face-foreground 'term-color-red t))))
  "Enabled USE flag"
  :group 'ebuild-tools)
(defface ebuild-useflag-disabled
  `((t
     :inherit default
     :weight bold
     :foreground ,(eval-when-compile (face-foreground 'term-color-blue t))))
  "Disabled USE flag"
  :group 'ebuild-tools)
#+end_src

*** eix-use-flags-last-installedversion
#+begin_src elisp :results none
(defun ebuild-tools-eix-use-flags-last-installedversion (catpkg)
  (with-temp-buffer
    (shmu-shell-command `((:my '"{last}<use*>{else}")
                          (eix :nocolor :pure-packages :exact
                               :format '"<installedversions:MY>"
                               : ,catpkg))
                        (current-buffer))
    (let ((almost
           (cons nil
                 (akater-misc-read-region-as-list (point-min) (point-max)))))
      (let ((sub almost))
        (while sub
          (if (memq (cadr sub) '(KERNEL= PYTHON_TARGETS=))
              (setf (cdr sub) (cdddr sub))
            (pop sub))))
      (cdr almost))))
#+end_src

** Definition
#+begin_src elisp :results none
(defun ebuild-tools-all-useflags (catpkg)
  (with-temp-buffer
    (let ((enabled
           ;; Disabled ones are prefixed here with dash
           ;; so only enabled ones will match this list.
           (ebuild-tools-eix-use-flags-last-installedversion catpkg)))
      (shmu-shell-command `((:my '"{last}<colliuse0>{else}")
                            (eix :nocolor :pure-packages :exact
                                 :format '"<availableversions:MY>"
                                 : ,catpkg))
                          (current-buffer))
      (mapcar (lambda (s)
                (setq s (symbol-name s)
                      s (fif (char-equal ?+ (aref s 0))
                          (lambda (x) (substring x 1))
                          s))
                (propertize s
                            'face (if (cl-member s enabled
                                                 :test #'string-equal
                                                 :key #'symbol-name)
                                      'ebuild-useflag-enabled
                                    'ebuild-useflag-disabled)))
              (cl-remove-if-not
               #'symbolp
               (akater-misc-read-region-as-list (point-min) (point-max)))))))
#+end_src

#+begin_src elisp :tangle no :results none
(defun ebuild-tools-all-useflags/nocolor (catpkg)
  (with-temp-buffer
    (shmu-shell-command `((:my '"{last}<colliuse0>{else}")
                          (eix :nocolor :pure-packages :exact
                               :format '"<availableversions:MY>"
                               : ,catpkg))
                        (current-buffer))
    (cl-remove-if-not #'symbolp
                      (akater-misc-read-region-as-list (point-min)
                                                       (point-max)))))
#+end_src

* ebuild-eclasses
** Summary
#+begin_src elisp :tangle no :load no :eval never
(ebuild-tools-ebuild-eclasses filename)
#+end_src
Presuming /filename/ is an ebuild file name, return eclasses that it uses, as list of symbols.

** Examples
*** Basic Examples
**** TEST-PASSED Get eclasses used in ebuild, as symbols
#+begin_src elisp :tangle no :results code :wrap example elisp :success-predicate #'consp
(ebuild-tools-ebuild-eclasses
 "/var/db/repos/gentoo/sys-apps/portage/portage-9999.ebuild")
#+end_src

#+EXPECTED:
#+begin_example elisp
(distutils-r1 linux-info toolchain-funcs tmpfiles prefix)
#+end_example

**** TEST-PASSED Do not make attempts at recognising non-ebuilds
#+begin_src elisp :tangle no :results code :wrap example elisp
(ebuild-tools-ebuild-eclasses "/etc/profile")
#+end_src

#+EXPECTED:
#+begin_example elisp
nil
#+end_example

** Prerequisites
*** ebuild-eclasses-of-buffer
**** Definition
#+begin_src elisp :results none
(defun ebuild-tools-ebuild-eclasses-of-buffer (&optional buffer)
  (with-current-buffer (setq buffer (or buffer (current-buffer)))
    (save-excursion
      (goto-char (point-min))
      (when (re-search-forward (rx bol "inherit "
                                   (group (zero-or-more not-newline))
                                   eol)
                               nil t)
        (akater-misc-read-region-as-list (match-beginning 1) (match-end 1))))))
#+end_src

** Definition
#+begin_src elisp :results none
(defun ebuild-tools-ebuild-eclasses (filename)
  (with-file-buffer filename
    (ebuild-tools-ebuild-eclasses-of-buffer)))
#+end_src

* ebuild-uses-eclass-p
** Summary
#+begin_src elisp :tangle no :load no :eval never
(ebuild-tools-ebuild-eclasses symbol ebuild)
#+end_src
Return non-nil when eclass /symbol/ is inherited in an ebuild file named /ebuild/.

** Examples
*** Basic Examples
**** TEST-PASSED Check whether ebuild uses eclass
#+begin_src elisp :tangle no :results code :wrap example elisp :success-predicate #'consp
(ebuild-tools-ebuild-uses-eclass-p 'libtool
  "/var/db/repos/gentoo/sys-apps/kmod/kmod-9999.ebuild")
#+end_src

#+EXPECTED:
#+begin_example elisp
(libtool bash-completion-r1)
#+end_example

** Definition
#+begin_src elisp :results none
(defun ebuild-tools-ebuild-uses-eclass-p (symbol ebuild)
  (declare (indent 1))
  (memq symbol (ebuild-tools-ebuild-eclasses ebuild)))
#+end_src

* get-variable
** Examples
*** Basic Examples
**** TEST-PASSED Get a variable defined in ebuild buffer
#+begin_src elisp :tangle no :results code :wrap example elisp
(with-file-buffer-read-only "/var/db/repos/gentoo/app-admin/pass/pass-9999.ebuild"
  (ebuild-tools-get-variable 'egit-repo-uri))
#+end_src

#+EXPECTED:
#+begin_example elisp
"\"https://git.zx2c4.com/password-store\""
#+end_example

** Definition
#+begin_src elisp :results none
(defun ebuild-tools-get-variable (sh-var-designator &optional buffer)
  "Quotes are preserved."
  (setq buffer (or buffer (current-buffer)))
  (let ((sh-var
         (lisp-style-symbol-name-to-unix-style-env-var-name
          (akater-misc-ensure-string sh-var-designator))))
    (do-logical-lines (:buffer buffer)
      (when (looking-at (rx line-start
                            (zero-or-more whitespace)
                            (literal sh-var)))
        (re-search-forward (rx "="))
        (cl-return-from do-logical-lines (buffer-substring-no-properties
                                          (point) (line-end-position)))))))
#+end_src

* find-log
** Dependencies
#+begin_src elisp :results none
(eval-when-compile (require 'ebuild-tools-macs))
(require 'simple)
(require 'compile)
(require 'ansi-color)
(require 'akater-misc-functional)
#+end_src

** TODO First, try to redirect to the directory specified in the log buffer
- State "TODO"       from              [2024-05-27 Mon 03:34]
  Locate the installed files only if it's empty.
** Definition
#+begin_src elisp :results none
(defun-ebuild-catpkg-logfile find-log ()
  "Visit a build log file of CATPKG."
  (with-current-buffer (get-buffer-create (format "*portage log: %s*" catpkg))
    (compilation-mode -1)
    ;; The order of these two operations should have been different,
    ;; I have no idea why it has to be like this.
    (read-only-mode -1)
    (erase-buffer)
    (insert-file-contents-literally logfile)
    (ansi-color-apply-on-region (point-min) (point-max))
    (when (string-prefix-p "app-emacs/" catpkg)
      (let ((pn (substring catpkg (length "app-emacs/"))))
        (ignore-errors
          (setq-local default-directory (file-name-directory
                                         (locate-library pn))
                      compilation-directory default-directory))))
    (compilation-mode 1)
    (read-only-mode 1)
    (switch-to-buffer (current-buffer))))
#+end_src
